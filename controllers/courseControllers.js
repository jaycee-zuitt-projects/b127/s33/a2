const course = require('../models/course');

//Creation of Course
/*
Steps:
1.) Create a conditional statement that will check if the user is and admin.
2.) Create a new Course object using the mongoose model and the information from the request body and the id from the header
3.) Save the new Course to the database

*/

module.exports.addCourse = (data) => {
	//if user is an admin
	if(data.isAdmin){
		let newCourse = new course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})
		return newCourse.save().then((course, error) => {
			//Course creation failed
			if(error){
				return false;
			}else{
				return true;
			}
		})

	}else{
		//if user is not an admin
		return false;
	}
}

//Retrieve all courses
module.exports.getAllCourses = () => {
	return course.find({}).then(result => {
		return result;
	})
}

//Retrieve all active
module.exports.getAllActive = () => {
	return course.find({isActive: true}).then(result => {
		return result;
	})
}

//Retrieve specific course
module.exports.specificCourse = (courseId) =>{
	return course.findById(courseId).then(result => {
		return result
	})
}


//update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	//findByIdAndUpdate(ID, updatesToBeApplied)
	return course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

	/*return course.findByIdAndUpdate(courseId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		}else{
			if(data.isAdmin){
				let updatedCourse = ({
					name: update.name,
					description: update.description,
					price: update.price
				})
					return updatedCourse.save().then(result => {
						return result
					})
			}
		}
	})

}*/
/*module.exports.updateCourse = (courseId, update, data) => {
	return course.findByIdAndUpdate(courseId).then(result => {
		if(data.isAdmin){
			let updatedCourse = ({
					name: update.name,
					description: update.description,
					price: update.price
				})
			return result.save(updatedCourse).then((result, error) => {
				if(error){
					return false;
				}else{
					return result
				}
			})
		}
	})
}


	/*if (data.isAdmin) {
		return course.findById(courseId).then((result,error)=>{
			if (error) {
				return false;
			} else {
				let updatedCourse = ({
					name: update.name,
					description: update.description,
					price: update.price
				})
				return result.save().then((updatedCourse, err)=>{
					if (err) {
						return false;
					} else {
						return updatedCourse;
					}
				})
			}
		})

	}*/

	/*Answer of Sir Paulo:
	module.exports.updateCourse = (req) => {
    return Course.findByIdAndUpdate({ _id: req.params.courseId }, 
        {   name: req.body.name, 
            description: req.body.description, 
            price: req.body.price 
        })
    .then(updatedCourse => { 
        return (updatedCourse) 
            ? { message: "Course update was successful", data:updatedCourse} 
            : { message: "Course update failed" }; })
    .catch(error => res.status(500).send({message: "Internal Server Error"}))
}
*/

//Ternary operato (es6 updates)
// ? = if condition
// : = else


//archive a course
/*Business logic
Steps:
1. check if admin(routes),
2. Create variable where the isActive will change into false
3. so we can use findByIdAndUpdate(id, updatedVariable). then error handling. if ourse is not archive, return false, if the course is archived successfully, return true.
*/

module.exports.archiveCourse = (reqParams) => {
	let updatedCourse = {
		isActive: false
	}
	return course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


