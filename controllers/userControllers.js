const User = require('../models/users');
const course = require('../models/course');
const bcrypt = require('bcryptjs');
const auth = require('../auth');

//Check if the email already exists

/*
1.) use mongoose "find" method to find duplicate emails
2.) use the "then" method to send a response back to the client based on the result of the find method(email already exists/not existing)
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		//The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			//no duplicate email found
			//the user is not yet registered in the database
			return false;
		}
	})
}

//user registration
/*
1.) Creates a new user object using the mongoose model and the information from the request body.
2.) Error handling, if error, return error. else, save the new user to the database.
*/


module.exports.registerUser = (reqBody) => {
	//uses the information from the request body to provide all necessary information
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password.
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	//saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error){
			return false;
		}else{
			//User registration is successful
			return true;
		}
	})
}


//User authentication
/*Steps:
1.) Check the database if the user email exists
2.) Compare the password provided in the login form with the password stored in the database
3.) Send/Generate a JSON web token if the user is successfully logged in and return false if not 
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		//If user does not exist
		if(result == null){
			return false
		}else{
			//if user exists
			//Create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			//"compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			//a good practice for a boolean variable/constanst is to use the word "is" or "are" at the beginning in the form of is+Noun
				//ex. isSingle, isDone, isAdmin, areDone, etc.
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//if the password match/result of the above code is true
			if(isPasswordCorrect){
				//Generate an access token.
				//Use the "createAccessToken" method defined in the "auth.js" file
				//returning an object back to the frontend
				//We will use the mongoose method "toObject" it converts the mongoose object into a plain javascript object.
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				//password do not match
				return false;
			}
		}
	})
}

//2. Create a getProfile controller method for retrieving the details of the user:
//Answer:
/*module.exports.getProfile = (user) => {
	return User.findById({_id: user._id}).then(result => {
		if (result == null){
			return false
		}else{
			result.password = ""
			return result
		}
	})
}*/
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		result.password = "";
		return result;
	})
}


/*module.exports.getProfile = (data) => {
	return User.findById({_id: data._id}).then(result => {
	result.password = ""
	return result
	})
}

*/

//enroll a user to a class or coruse
/*Steps:
1. Find the document in the database using the users ID
2. Add the course ID to the user's enrollment array
3. Save the data in the database
4. Find the document in the database using the course's ID
5. add the User ID to the courses enrollees array
6. save the data in the database
7. Error handling: if successful, return true else return false
*/

//Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.
module.exports.enroll = async (data) => {
	//Create an "isUserUpdated" variables and returns true upon successful update otherwise false
	//using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the client 
	let isUserUpdated = await User.findById(data.userId).then(user => {
		//Add the courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId})
	

	//Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	})//the isUserUpdated tells us if the course was successfully added to the user

	let isCourseUpdated = await course.findById(data.courseId).then(course => {
		//add the userid in the course's enrollees array
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})//the isCourseUpdated tells us if the user was successfully saved to the course
	})
	//Condition that will check if the user and course documents have been successfully updated
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		//if update is failed
		return false;
	}
}

