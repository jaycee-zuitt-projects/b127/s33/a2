const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');

//Route for creating a course
//localhost:4000/courses/
router.post('/', auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));
})

//Retrieve all courses
router.get('/all', (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
})

//Retrieve all active courses
router.get('/', (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})

//Retrieve course by ID
router.get("/:courseId", (req,res)=>{
	courseController.specificCourse(req.params.id)
	.then(result =>{
		res.send(result)
	})
})

//Update a course
/*router.put('/:courseId', auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.updateCourse(req.params.id, req.body, data).then(result => res.send(result))
})*/

/*//courseRoutes

router.put('/:courseId', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	courseController.updateCourse(req.params.courseId, req.body, data).then(result => res.send(result))
})
*/
// Answer of Sir Paulo:
router.put('/:courseId', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        courseController.updateCourse(req.params, req.body).then(result => res.send(result));
    }else{
        res.send(false)
    }
    
    
})


//archive a course/soft delete a course

router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	courseController.archiveCourse(req.params).then(result => res.send(result))
	}else{
		res.send(false)
	}
})




module.exports = router;